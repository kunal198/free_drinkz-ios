//
//  Messages.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 27/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit

class Messages: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var messageTbl: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:MessagesCell = messageTbl.dequeueReusableCell(withIdentifier: "MessagesCell") as! MessagesCell
        
        if (indexPath.row%5==0) {
            cell.postImg.image = UIImage(named: "testpic1")
            cell.nameLbl.text="Simona"
        } else if (indexPath.row%5==1) {
            cell.postImg.image = UIImage(named: "testpic2")
            cell.nameLbl.text="Dianna"
        } else if (indexPath.row%5==2) {
            cell.postImg.image = UIImage(named: "testpic3.jpg")
            cell.nameLbl.text="Clarke"
        } else if (indexPath.row%5==3) {
            cell.postImg.image = UIImage(named: "testpic4")
            cell.nameLbl.text="Garry"
        } else {
            cell.postImg.image = UIImage(named: "testpic5")
            cell.nameLbl.text="Elle Waston"
        }
        cell.postImg.layer.cornerRadius = 4
        cell.postImg.clipsToBounds = true
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }
    
    // MARK: Buttons Action
    
    @IBAction func Back(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

  
}
