//
//  Slidebar.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 25/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit

class Slidebar: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var slidebarTbl: UITableView!
    
    var nameArray = [[String : String]]()
    var airports = [[String : String]]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
       
         nameArray = [["name": "Me", "image": "me"],["name": "Menu", "image": "menu"],["name": "My Orders", "image": "orders"],["name": "Messages", "image": "messages"],["name": "I'm Buying Request", "image": "i'mbuy"],["name": "Who's Buying Request", "image": "who'sbuy"],["name": "Payment", "image": "sideicon"],["name": "Contact", "image": "contact"],["name": "About", "image": "about"],["name": "Logout", "image": "logout"]]
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return nameArray.count
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:SlidebarCell = slidebarTbl.dequeueReusableCell(withIdentifier: "Slidebar") as! SlidebarCell
        cell.backgroundColor = UIColor.init(colorLiteralRed: 36/255, green: 69/255, blue: 138/255, alpha: 1)
        cell.nameLbl.text = nameArray[indexPath.row]["name"]
        cell.itemImg.image = UIImage(named: nameArray[indexPath.row]["image"]!)
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        tableView.reloadData()
        let cell = tableView.cellForRow(at: indexPath as IndexPath) as! SlidebarCell
        cell.backgroundColor = UIColor.init(colorLiteralRed: 26/255, green: 45/255, blue: 115/255, alpha: 1)
        if (cell.nameLbl.text == "Logout") {
            print("logout")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Login") as! Login
            navigationController?.pushViewController(vc,animated: true)
        } else if (cell.nameLbl.text == "My Orders") {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "MyOrders") as! MyOrders
            navigationController?.pushViewController(vc,animated: true)
        } else if (cell.nameLbl.text == "Me") {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Profile") as! Profile
            navigationController?.pushViewController(vc,animated: true)
        } else if (cell.nameLbl.text == "Messages") {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Messages") as! Messages
            navigationController?.pushViewController(vc,animated: true)
        } else if (cell.nameLbl.text == "Contact") {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "Contact") as! Contact
            navigationController?.pushViewController(vc,animated: true)
        }


    }
    
       override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}
