//
//  Profile.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 25/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import SideMenu

class Profile: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var profileTbl: UITableView!
    @IBOutlet weak var sendMsgBtn: UIButton!
    @IBOutlet weak var sendFoodBtn: UIButton!
    @IBOutlet weak var sendView: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()

        sendMsgBtn.layer.cornerRadius=21
        sendMsgBtn.clipsToBounds=true

        sendFoodBtn.layer.cornerRadius=21
        sendFoodBtn.layer.borderWidth = 1
        sendFoodBtn.layer.borderColor = sendFoodBtn.titleLabel?.textColor.cgColor
        sendFoodBtn.clipsToBounds=true
        

        
        setupSideMenu()

    }
    fileprivate func setupSideMenu() {
        // Define the menus
        SideMenuManager.menuLeftNavigationController = storyboard!.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        
        // Enable gestures. The left and/or right menus must be set up above for these to work.
        // Note that these continue to work on the Navigation Controller independent of the View Controller it displays!
        SideMenuManager.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: self.navigationController!.view)
    
        
        // Set up a cool background image for demo purposes
   //     SideMenuManager.menuAnimationBackgroundColor = UIColor(patternImage: UIImage(named: "background")!)
        SideMenuManager.menuAnimationBackgroundColor = UIColor(white: 1, alpha: 0.0)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:PostCell = profileTbl.dequeueReusableCell(withIdentifier: "PostCell") as! PostCell

        if (indexPath.row%5==0) {
            cell.postTypeImg.image = UIImage(named: "fbgray")
            cell.postImg.image = UIImage(named: "testpic1")
            cell.nameLbl.text="Simona"
        } else if (indexPath.row%5==1) {
            cell.postTypeImg.image = UIImage(named: "fbgray")
            cell.postImg.image = UIImage(named: "testpic2")

            cell.nameLbl.text="Dianna"
        } else if (indexPath.row%5==2) {
            cell.postTypeImg.image = UIImage(named: "twittergray")
            cell.postImg.image = UIImage(named: "testpic3.jpg")
            cell.nameLbl.text="Clarke"
        } else if (indexPath.row%5==3) {
            cell.postTypeImg.image = UIImage(named: "g+gray")
            cell.postImg.image = UIImage(named: "testpic4")
            cell.nameLbl.text="Garry"
        } else {
            cell.postTypeImg.image = UIImage(named: "twittergray")
            cell.postImg.image = UIImage(named: "testpic5")
            cell.nameLbl.text="Elle Waston"
        }
        cell.postImg.layer.cornerRadius = 4
        cell.postImg.clipsToBounds = true
        
        return cell
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
      
    }

       override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}
