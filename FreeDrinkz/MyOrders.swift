//
//  MyOrders.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 27/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit

class MyOrders: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var receivedLbl: UILabel!
    @IBOutlet weak var sentLbl: UILabel!
    @IBOutlet weak var receivedBtn: UIButton!
    @IBOutlet weak var sentBtn: UIButton!
    @IBOutlet weak var ordersTbl: UITableView!
    @IBOutlet weak var coupanView: UIView!
    @IBOutlet weak var coupanView1: UIView!
    var typeStr = String()
    override func viewDidLoad() {
        super.viewDidLoad()

        
        backView.layer.cornerRadius = 16
        backView.layer.borderColor = UIColor.white.cgColor
        backView.layer.borderWidth = 1
        backView.clipsToBounds = true
        typeStr = "received"
        coupanView.isHidden = true
        coupanView1.layer.cornerRadius = 4
        coupanView1.clipsToBounds = true
    }

    //Mark: UITableView Methods
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 16
    }
    
    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:OrderCell = ordersTbl.dequeueReusableCell(withIdentifier: "OrderCell") as! OrderCell
        if (typeStr == "received") {
            cell.fromToLbl.text = "From:"
        } else {
            cell.fromToLbl.text = "To:"
        }
        cell.completedView.isHidden = true
        cell.backView1.isHidden = true

        if (indexPath.row%4==0) {
            cell.drinkImg.image = UIImage(named: "drink1")
            cell.userImg.image = UIImage(named: "testpic1")
            
            cell.drinkLbl.text="Rosemary Smash"
        } else if (indexPath.row%4==1) {
            cell.drinkImg.image = UIImage(named: "drink2.jpg")
            cell.userImg.image = UIImage(named: "testpic2")
            cell.drinkLbl.text="Blueberry Smash"
        } else if (indexPath.row%4==2) {
            cell.drinkImg.image = UIImage(named: "drink3")
            cell.userImg.image = UIImage(named: "testpic5")
            cell.drinkLbl.text="Lemonade"
            cell.completedView.isHidden = false
            cell.backView1.isHidden = false
        } else {
            cell.drinkImg.image = UIImage(named: "drink4")
            cell.userImg.image = UIImage(named: "testpic4")
            cell.drinkLbl.text="Lavender Lemonade"
        }
        
        cell.userImg.layer.cornerRadius = cell.userImg.frame.size.height/2
        cell.userImg.clipsToBounds = true
        cell.drinkImg.layer.cornerRadius = 4
        cell.drinkImg.clipsToBounds = true
        cell.backView.layer.cornerRadius = 4
        cell.backView.layer.masksToBounds = false;
        cell.backView.layer.shadowColor = UIColor.black.cgColor
        cell.backView.layer.shadowOpacity = 0.3
        cell.backView.layer.shadowRadius = 2
        cell.backView.layer.shadowOffset = CGSize(width: 1, height: 1)
        cell.completedView.layer.cornerRadius = 10
        cell.completedView.clipsToBounds = true
       
        cell.coupanViewBtn.addTarget(self, action: #selector(viewCoupan(_:)), for: .touchDown)

        
        return cell
    }
    
    func viewCoupan(_ button: UIButton) {
        coupanView.isHidden = false
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
    }

    //MARK: Button Actions
    @IBAction func Cancel(_ sender: Any) {
        coupanView.isHidden = true
    }
    @IBAction func ReceivedSent(_ sender: Any) {
        if ((sender as AnyObject).tag == 0) {
            receivedBtn.backgroundColor = UIColor.white
            receivedLbl.textColor = UIColor(red: 217/255, green: 41/255, blue: 39/255, alpha: 1)
            sentBtn.backgroundColor = UIColor(red: 217/255, green: 41/255, blue: 39/255, alpha: 1)
            sentLbl.textColor = UIColor.white
            typeStr = "received"
        } else if ((sender as AnyObject).tag == 1) {
            sentBtn.backgroundColor = UIColor.white
            sentLbl.textColor = UIColor(red: 217/255, green: 41/255, blue: 39/255, alpha: 1)
            receivedBtn.backgroundColor = UIColor(red: 217/255, green: 41/255, blue: 39/255, alpha: 1)
            receivedLbl.textColor = UIColor.white
            typeStr = "sent"
        }
        ordersTbl.reloadData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    
}
