//
//  Login.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 24/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit

class Login: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var emailtxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var signUpBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationController?.isNavigationBarHidden = true
        
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border.frame = CGRect(x: 0, y: emailtxt.frame.size.height - width, width:  emailtxt.frame.size.width, height: emailtxt.frame.size.height)
        border.borderWidth = width
        emailtxt.layer.addSublayer(border)
        emailtxt.layer.masksToBounds = true
     
        let border1 = CALayer()
        border1.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border1.frame = CGRect(x: 0, y: passwordTxt.frame.size.height - width, width:  passwordTxt.frame.size.width, height: passwordTxt.frame.size.height)
        border1.borderWidth = width
        passwordTxt.layer.addSublayer(border1)
        passwordTxt.layer.masksToBounds = true

        
        emailtxt .setValue(UIColor.lightGray, forKeyPath: "_placeholderLabel.textColor")
        passwordTxt .setValue(UIColor.lightGray, forKeyPath: "_placeholderLabel.textColor")
       
        signInBtn.layer.cornerRadius=22
        signInBtn.clipsToBounds=true
        
        let fontSize: CGFloat = 16.0
        let fontFamily: String = "FuturaBT-LightItalic"
        let attrString = NSMutableAttributedString(attributedString: signUpBtn.currentAttributedTitle!)
        attrString.addAttribute(NSFontAttributeName, value: UIFont(name: fontFamily, size: fontSize)!, range: NSMakeRange(0, attrString.length))
        signUpBtn.setAttributedTitle(attrString, for: .normal)

        let panGesture = UIPanGestureRecognizer(target: self, action:(#selector(handlePanGesture(panGesture:))))
        self.view.addGestureRecognizer(panGesture)
        
    }
    
    func handlePanGesture(panGesture: UIPanGestureRecognizer) {
        // get translation
        if panGesture.state == .began || panGesture.state == .changed {
        }
    }
    
    // MARK: Buttons Action
    
    @IBAction func SignIn(_ sender: Any) {
    }
    @IBAction func ForgorPassword(_ sender: Any) {
    }
    @IBAction func SignUp(_ sender: Any) {
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        emailtxt.resignFirstResponder()
        passwordTxt.resignFirstResponder()
    }
    
    // MARK: Text Field Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool  {
        textField.resignFirstResponder()
        return true;
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

