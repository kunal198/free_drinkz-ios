//
//  Register.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 24/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit

class Register: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var scrView: UIScrollView!
    @IBOutlet weak var firstNameTxt: UITextField!
    @IBOutlet weak var lastNameTxt: UITextField!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var confirmPasswordTxt: UITextField!
    @IBOutlet weak var stateTxt: UITextField!
    @IBOutlet weak var cityTxt: UITextField!
    @IBOutlet weak var zipCodeTxt: UITextField!
    @IBOutlet weak var ageTxt: UITextField!
    @IBOutlet weak var maleTxt: UITextField!
    @IBOutlet weak var signInBtn: UIButton!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var choiceView: UIView!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var signInBtn2: UIButton!
    @IBOutlet weak var memberImg: UIImageView!
    @IBOutlet weak var placeImg: UIImageView!
    @IBOutlet weak var artistImg: UIImageView!
    @IBOutlet weak var memberBtn: UIButton!
    @IBOutlet weak var placeBtn: UIButton!
    @IBOutlet weak var artistBtn: UIButton!
    @IBOutlet weak var memberLbl: UILabel!
    @IBOutlet weak var placeLbl: UILabel!
    @IBOutlet weak var artistLbl: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border.frame = CGRect(x: 0, y: firstNameTxt.frame.size.height - width, width:  firstNameTxt.frame.size.width, height: firstNameTxt.frame.size.height)
        border.borderWidth = width
        firstNameTxt.layer.addSublayer(border)
        firstNameTxt.layer.masksToBounds = true
        
        
        let border1 = CALayer()
        border1.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border1.frame = CGRect(x: 0, y: lastNameTxt.frame.size.height - width, width:  lastNameTxt.frame.size.width, height: lastNameTxt.frame.size.height)
        border1.borderWidth = width
        lastNameTxt.layer.addSublayer(border1)
        lastNameTxt.layer.masksToBounds = true
        
        
        
        let border2 = CALayer()
        border2.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border2.frame = CGRect(x: 0, y: emailTxt.frame.size.height - width, width:  emailTxt.frame.size.width, height: emailTxt.frame.size.height)
        border2.borderWidth = width
        emailTxt.layer.addSublayer(border2)
        emailTxt.layer.masksToBounds = true
        
        
        let border3 = CALayer()
        border3.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border3.frame = CGRect(x: 0, y: passwordTxt.frame.size.height - width, width:  passwordTxt.frame.size.width, height: passwordTxt.frame.size.height)
        border3.borderWidth = width
        passwordTxt.layer.addSublayer(border3)
        passwordTxt.layer.masksToBounds = true
        
        
        let border4 = CALayer()
        border4.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border4.frame = CGRect(x: 0, y: confirmPasswordTxt.frame.size.height - width, width:  confirmPasswordTxt.frame.size.width, height: confirmPasswordTxt.frame.size.height)
        border4.borderWidth = width
        confirmPasswordTxt.layer.addSublayer(border4)
        confirmPasswordTxt.layer.masksToBounds = true
        
        
        let border5 = CALayer()
        border5.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border5.frame = CGRect(x: 0, y: stateTxt.frame.size.height - width, width:  stateTxt.frame.size.width, height: stateTxt.frame.size.height)
        border5.borderWidth = width
        stateTxt.layer.addSublayer(border5)
        stateTxt.layer.masksToBounds = true
        
        
        let border6 = CALayer()
        border6.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border6.frame = CGRect(x: 0, y: cityTxt.frame.size.height - width, width:  cityTxt.frame.size.width, height: cityTxt.frame.size.height)
        border6.borderWidth = width
        cityTxt.layer.addSublayer(border6)
        cityTxt.layer.masksToBounds = true
        
        
        let border7 = CALayer()
        border7.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border7.frame = CGRect(x: 0, y: zipCodeTxt.frame.size.height - width, width:  zipCodeTxt.frame.size.width, height: zipCodeTxt.frame.size.height)
        border7.borderWidth = width
        zipCodeTxt.layer.addSublayer(border7)
        zipCodeTxt.layer.masksToBounds = true
        
        let border8 = CALayer()
        border8.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border8.frame = CGRect(x: 0, y: ageTxt.frame.size.height - width, width:  ageTxt.frame.size.width, height: ageTxt.frame.size.height)
        border8.borderWidth = width
        ageTxt.layer.addSublayer(border8)
        ageTxt.layer.masksToBounds = true
        
        let border9 = CALayer()
        border9.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border9.frame = CGRect(x: 0, y: maleTxt.frame.size.height - width, width:  maleTxt.frame.size.width, height: maleTxt.frame.size.height)
        border9.borderWidth = width
        maleTxt.layer.addSublayer(border9)
        maleTxt.layer.masksToBounds = true
        
        
        let border10 = CALayer()
        border10.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border10.frame = CGRect(x: 0, y: memberBtn.frame.size.height - width, width:  memberBtn.frame.size.width, height: memberBtn.frame.size.height)
        border10.borderWidth = width
        memberBtn.layer.addSublayer(border10)
        memberBtn.layer.masksToBounds = true
        
        
        let border11 = CALayer()
        border11.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border11.frame = CGRect(x: 0, y: placeBtn.frame.size.height - width, width:  placeBtn.frame.size.width, height: placeBtn.frame.size.height)
        border11.borderWidth = width
        placeBtn.layer.addSublayer(border11)
        placeBtn.layer.masksToBounds = true
        
        let border12 = CALayer()
        border12.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border12.frame = CGRect(x: 0, y: artistBtn.frame.size.height - width, width:  artistBtn.frame.size.width, height: artistBtn.frame.size.height)
        border12.borderWidth = width
        artistBtn.layer.addSublayer(border12)
        artistBtn.layer.masksToBounds = true
        
        
        firstNameTxt .setValue(UIColor.lightGray, forKeyPath: "_placeholderLabel.textColor")
        lastNameTxt .setValue(UIColor.lightGray, forKeyPath: "_placeholderLabel.textColor")
        emailTxt .setValue(UIColor.lightGray, forKeyPath: "_placeholderLabel.textColor")
        passwordTxt .setValue(UIColor.lightGray, forKeyPath: "_placeholderLabel.textColor")
        confirmPasswordTxt .setValue(UIColor.lightGray, forKeyPath: "_placeholderLabel.textColor")
        stateTxt .setValue(UIColor.lightGray, forKeyPath: "_placeholderLabel.textColor")
        cityTxt .setValue(UIColor.lightGray, forKeyPath: "_placeholderLabel.textColor")
        zipCodeTxt .setValue(UIColor.lightGray, forKeyPath: "_placeholderLabel.textColor")
        ageTxt .setValue(UIColor.lightGray, forKeyPath: "_placeholderLabel.textColor")
        maleTxt .setValue(UIColor.lightGray, forKeyPath: "_placeholderLabel.textColor")

        
        
        
        registerBtn.layer.cornerRadius=21
        registerBtn.clipsToBounds=true
        
        nextBtn.layer.cornerRadius=21
        nextBtn.clipsToBounds=true
        
        
        let fontSize: CGFloat = 16.0
        let fontFamily: String = "FuturaBT-LightItalic"
        let attrString = NSMutableAttributedString(attributedString: signInBtn.currentAttributedTitle!)
        attrString.addAttribute(NSFontAttributeName, value: UIFont(name: fontFamily, size: fontSize)!, range: NSMakeRange(0, attrString.length))
        signInBtn.setAttributedTitle(attrString, for: .normal)
        signInBtn2.setAttributedTitle(attrString, for: .normal)
        
        
        if (UIScreen.main.bounds.size.height==568 || UIScreen.main.bounds.size.height==480) {
            scrView.contentSize = CGSize(width: 320,height: 600)
        }
        
        
        
        
        let panGesture = UIPanGestureRecognizer(target: self, action:(#selector(handlePanGesture(panGesture:))))
        self.view.addGestureRecognizer(panGesture)
        
    }
    
    func handlePanGesture(panGesture: UIPanGestureRecognizer) {
        // get translation
        if panGesture.state == .began || panGesture.state == .changed {
        }
    }

    
    @IBAction func Photo(_ sender: Any) {
    }
    @IBAction func Next(_ sender: Any) {
        choiceView.isHidden=true
    }
    @IBAction func Choice(_ sender: Any) {
        if ((sender as AnyObject).tag == 0) {
            memberImg.image = UIImage(named: "red")
            placeImg.image = UIImage(named: "gray")
            artistImg.image = UIImage(named: "gray")
            
        } else if ((sender as AnyObject).tag == 1) {
            placeImg.image = UIImage(named: "red")
            memberImg.image = UIImage(named: "gray")
            artistImg.image = UIImage(named: "gray")
            
        } else if ((sender as AnyObject).tag == 2) {
            artistImg.image = UIImage(named: "red")
            placeImg.image = UIImage(named: "gray")
            memberImg.image = UIImage(named: "gray")
            
        }
        
    }
    @IBAction func Register(_ sender: Any) {
    }
    @IBAction func Back(_ sender: Any) {
        if ((sender as AnyObject).tag == 1) {
            choiceView.isHidden = false
        }else {
            _ = navigationController?.popViewController(animated: true)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        emailTxt.resignFirstResponder()
        passwordTxt.resignFirstResponder()
    }
    
    // MARK: Text Field Delegate
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool  {
        textField.resignFirstResponder()
        return true;
    }
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        scrView.isScrollEnabled = false
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        scrView.isScrollEnabled = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
}
