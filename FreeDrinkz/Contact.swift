//
//  Contact.swift
//  FreeDrinkz
//
//  Created by Brst-Pc109 on 27/03/17.
//  Copyright © 2017 Brst-Pc109. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class Contact: UIViewController {
    @IBOutlet var messageTxt: IQTextView!
    @IBOutlet weak var emailTxt: UITextField!
    @IBOutlet weak var phoneTxt: UITextField!
    @IBOutlet weak var nameTxt: UITextField!
    @IBOutlet weak var sendBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()


        let border = CALayer()
        let width = CGFloat(1.0)
        border.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border.frame = CGRect(x: 0, y: nameTxt.frame.size.height - width, width:  nameTxt.frame.size.width, height: nameTxt.frame.size.height)
        border.borderWidth = width
        nameTxt.layer.addSublayer(border)
        nameTxt.layer.masksToBounds = true
        
        
        let border1 = CALayer()
        border1.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border1.frame = CGRect(x: 0, y: phoneTxt.frame.size.height - width, width:  phoneTxt.frame.size.width, height: phoneTxt.frame.size.height)
        border1.borderWidth = width
        phoneTxt.layer.addSublayer(border1)
        phoneTxt.layer.masksToBounds = true
        
        
        
        let border2 = CALayer()
        border2.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border2.frame = CGRect(x: 0, y: emailTxt.frame.size.height - width, width:  emailTxt.frame.size.width, height: emailTxt.frame.size.height)
        border2.borderWidth = width
        emailTxt.layer.addSublayer(border2)
        emailTxt.layer.masksToBounds = true
        
        
        let border3 = CALayer()
        border3.borderColor = UIColor.lightGray.withAlphaComponent(0.5).cgColor
        border3.frame = CGRect(x: 0, y: messageTxt.frame.size.height - width, width:  messageTxt.frame.size.width, height: messageTxt.frame.size.height)
        border3.borderWidth = width
        messageTxt.layer.addSublayer(border3)
        messageTxt.layer.masksToBounds = true

    
        nameTxt .setValue(UIColor.lightGray, forKeyPath: "_placeholderLabel.textColor")
        phoneTxt .setValue(UIColor.lightGray, forKeyPath: "_placeholderLabel.textColor")
        emailTxt .setValue(UIColor.lightGray, forKeyPath: "_placeholderLabel.textColor")

        sendBtn.layer.cornerRadius=22
        sendBtn.clipsToBounds=true

    }
    @IBAction func Back(_ sender: Any) {
        _ = navigationController?.popViewController(animated: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

   
}
